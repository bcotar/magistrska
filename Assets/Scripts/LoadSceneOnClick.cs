﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadByIndex (int sceneIndex){

		if (sceneIndex == 1) { //Before we go to play check if game was SETUP.
			try {
				String jsonString = File.ReadAllText (Constants.pathJson);	
				SceneManager.LoadScene (sceneIndex);	
			} catch (System.Exception e) {
				AndroidHelper.ShowAndroidToastMessage ("Game not SETUP. Please go to SETUP.");
				Debug.Log ("Could not open Data.json: " + e.Message);
			}
		}else{
			SceneManager.LoadScene (sceneIndex);	
		}


	}
}
