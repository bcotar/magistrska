﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.SceneManagement;

public class CameraManager_v2 : MonoBehaviour {

	private bool camAvailable;
	private WebCamTexture backCam;
	private Texture defaultBackground;

	public RawImage background;
	public AspectRatioFitter fit;
	public GameObject photoButton;
	public GameObject okButton;
	public Text tekst;
	//public RawImage newTexture;

	private bool backIndex;
	private bool isRunning;
	float ratio;

	// Use this for initialization
	void Start () {
		backIndex = false;
		defaultBackground = background.texture;
		WebCamDevice[] devices = WebCamTexture.devices;

		if (devices.Length == 0) {
			Debug.Log ("No camera detected");
			camAvailable = false;
			return;
		}

		if(!devices[0].isFrontFacing){
			backCam = new WebCamTexture (devices[0].name, Screen.height, Screen.width);
		}


		if (backCam == null) {
			Debug.Log ("Unable to find back camera");
			return;
		}

		backCam.Play();
		background.texture = backCam;

		camAvailable = true;
		ratio = (float)backCam.width / (float)backCam.height;
		//ratio = ratio * 1.77778f; //This scale correction is hack no clue why it is requred

	}

	// Update is called once per frame
	void Update () {
		if(!camAvailable){
			return;
		}

		if (Input.GetKey(KeyCode.Escape)) {
			SceneManager.LoadScene(0);
		}

		background.rectTransform.localScale = new Vector3 (1f, ratio, 1f);
		int orient = -backCam.videoRotationAngle;
		background.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);
	}

	public void PauseCamera(){
		backCam.Pause();
		backIndex = true;
		try{
			// 7.1.2018
			//Color[] c = backCam.GetPixels (41, 172 , 1076, 605);
			//Texture2D thumbTex = new Texture2D (1076, 605, TextureFormat.RGB24, false); //our texture is a square with content in the bottom part.
			//thumbTex.SetPixels (0, 0, 1076, 605, c);
			//thumbTex.Apply ();

			//Play_v2 - zakomentirano, pri uporabi te teksture, se odkomentira
			//Texture2D thumbTex = new Texture2D (backCam.width, backCam.height, TextureFormat.RGB24, false); //our texture is a square with content in the boottom part.
			//thumbTex.SetPixels (backCam.GetPixels());
			//thumbTex.Apply ();

			//newTexture.texture = thumbTex;
			Debug.Log ("Thumb Texture loaded");
		}catch (Exception e){
			Debug.Log ("Thumb Texture not loaded: " + e.Message);
		}
	}

	public void GoBack(){
		if(backIndex){
			backCam.Play();
			okButton.SetActive(false);
			photoButton.SetActive(true);
			backIndex = false;
		}
		else{
			backCam.Stop();
			SceneManager.LoadScene (0);
		}
	}

	public void TakePicture(){
		StartCoroutine(CaptureImage());
	}

	IEnumerator CaptureImage(){
		
		isRunning = true;
		yield return new WaitForEndOfFrame ();

		//7.1.2018
		/*
		Color[] c = backCam.GetPixels (41, 172 , 1076, 605);

		Texture2D destTex = new Texture2D (1076, 1076, TextureFormat.ARGB32, false); //our texture is a square with content in the boottom part.
		destTex.SetPixels (0, 0, 1076, 605, c);
		destTex.Apply ();

		Texture2D thumbTex = new Texture2D (1076, 605, TextureFormat.RGB24, false); //our texture is a square with content in the boottom part.
		thumbTex.SetPixels (0, 0, 1076, 605, c);
		thumbTex.Apply ();


		System.IO.File.WriteAllBytes (Constants.pathTexture, destTex.EncodeToPNG());
		//AndroidHelper.ShowAndroidToastMessage("Puzzle saved");
		System.IO.File.WriteAllBytes (Constants.pathTextureThumb, thumbTex.EncodeToPNG());
		Debug.Log ("Puzzle saved to: " + Constants.pathTexture);
		isRunning = false;
		backCam.Stop();//You need to stop camera in order to use it later in project.

		try{
			newTexture.texture = thumbTex;
			Debug.Log ("Thumb Texture loaded");
		}catch (Exception e){
			Debug.Log ("Thumb Texture not loaded: " + e.Message);
		}
		*/
		Texture2D newTexture = new Texture2D (backCam.width, backCam.height, TextureFormat.ARGB32, false); //our texture is a square with content in the boottom part.
		newTexture.SetPixels (backCam.GetPixels());
		newTexture.Apply ();

		//Texture2D thumbTex = new Texture2D (backCam.width, backCam.height, TextureFormat.RGB24, false); //our texture is a square with content in the boottom part.
		//thumbTex.SetPixels (backCam.GetPixels());
		//thumbTex.Apply ();

		System.IO.File.WriteAllBytes (Constants.newTexture, newTexture.EncodeToPNG());
		AndroidHelper.ShowAndroidToastMessage("Puzzle saved");
		//System.IO.File.WriteAllBytes (Constants.pathTextureThumb, thumbTex.EncodeToPNG());
		//Debug.Log ("Puzzle saved to: " + Constants.pathTexture);
		isRunning = false;
		backCam.Stop();//You need to stop camera in order to use it later in project.

		try{
			Debug.Log ("Thumb Texture loaded");
		}catch (Exception e){
			Debug.Log ("Thumb Texture not loaded: " + e.Message);
		}
			
	}

}