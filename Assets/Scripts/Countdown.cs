﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

	public Text text;
	public Button button;
	private Countdown skripta;

	float timeLeft = 5.0f;

	void Start () {
		skripta = GetComponent<Countdown>();
	}

	void Update() {
		timeLeft -= Time.deltaTime;
		text.text = ((int)timeLeft).ToString();
		if( timeLeft < 1) {
			text.gameObject.SetActive (false);
			button.gameObject.SetActive (true);
			skripta.enabled = false;
		}
	}
}
